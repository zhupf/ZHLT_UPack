#if !defined(AFX_COMPRESS_H__C23FA1C1_753B_11D2_8483_5254ABDD2B69__INCLUDED_)
#define AFX_COMPRESS_H__C23FA1C1_753B_11D2_8483_5254ABDD2B69__INCLUDED_

#include <windows.h>

//压缩算法
#define	GL_ZIPMETHOD_NONE				-2			//不压缩
#define	GL_ZIPMETHOD_DEFAULT			-1			//默认压缩算法
#define	GL_ZIPMETHOD_FAST				1			//较快压缩算法
#define	GL_ZIPMETHOD_FASTEST			2			//最快速压缩算法
#define	GL_ZIPMETHOD_BEST				9			//最高压缩率算法



//压缩数据
//
//参数：
//	pDst				压缩后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳压缩的数据,最坏情况下MAXDestLen=ulSrcLen*0.01+12字节
//	pSrc				待压缩数据地址
//	lenSrc				待压缩数据长度
//	nCompressMethod		压缩算法，-1默认压缩级别，0不压缩，1较快速度压缩，2最快压缩方式，9最高压缩率
//
//	返回：
//	0					失败
//	其它				压缩后数据长度
typedef int (*gpCompressFunc)(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc,int nCompressMethod);



//解压数据
//
//参数：
//	pDst				解压后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳解压的数据
//	pSrc				待解压数据地址
//	lenSrc				待解压数据长度
//
//	返回：
//	0					失败
//	其它				解压后数据长度
typedef int (*gpUncompressFunc)(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc);

/// zlib example

//int gpCompress(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc,int nCompressMethod)
//{
//	uLongf len = lenDst;
//	int n = compress2((Bytef *)pDst,&len,(const Bytef *)pSrc,lenSrc,nCompressMethod);
//	lenDst = len;
//	return n;
//}

//int gpUncompress(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc)
//{
//	uLongf len = lenDst;
//	int n = uncompress((Bytef *)pDst,&len,(const Bytef *)pSrc,lenSrc);
//	lenDst = len;
//	return n;
//}


//压缩数据
//
//调用：
//	pBuf			需要压缩的数据地址
//	lenBuf			调用时是待压缩数据的长度，若压缩成功返回时是压缩后的数据长度
//	nZipMethod		压缩方法
//	cnTopReserve	在压缩后的数据缓冲区前部保留多少个字节的空间，留作其它用途
//
//返回：
//	若不压缩或者申请内存失败或压缩失败，返回NULL，其它表示压缩后的缓冲区地址，调用者需要释放这块缓冲区
void *gpZipBuf(gpCompressFunc pFunc, void *pBuf,int &lenBuf,int nZipMethod=GL_ZIPMETHOD_DEFAULT,int cnTopReserve=0);

//解压缩数据
//
//调用：
//	pBuf		压缩数据地址，必须是gpZipBuf函数生成的压缩数据
//	lenBuf		调用时是压缩数据的长度，若解压成功返回解压后的数据长度
//
//返回：
//	当解压缩成功时返回解压数据的地址，否则返回NULL，解压后的数据需要调用者释放
void *gpUnzipBuf(gpUncompressFunc pFunc, void *pBuf,int &lenBuf);

#endif // !defined(AFX_COMPRESS_H__C23FA1C1_753B_11D2_8483_5254ABDD2B69__INCLUDED_)
