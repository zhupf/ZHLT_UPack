#pragma once

#include <map>
#include <string>
/*****************************************************************************\
PackageFile.h
创建: 
检查: （姓名） （日期）
说明：(类的功能说明，使用说明)
	  名称: CPackageFile
	  功能：对文件进行打包，支持文件压缩选项
	  附注：

更改记录-----------------------------------------------------------------------
修改:（日期）  （姓名）  （内容：修改或添加的函数或结构名称）


	  2004-05-25  版本变为1.1 在包文件头加入了编辑日期时间，同时在
	                      索引文件中加入了版本及包更新时间，如果索引创建时
						  间与包创建时间不一到，重建索引文件，支持包从1.0 
						  格式升级到1.1格式-UpdatePacketFile();
	  2004-07-08  文件完整性检查，不完整的文件被丢弃。

	  2011-01-19  版本变更为1.2 设计为可扩展的模式，增加文件信息在末尾

检查:（日期）  （姓名）  （内容）


附加说明：
	类库创建包文件（.gls）文件后还会创建索引文件(.idx)
	为提高读写、删除、替换速度，不要频繁执行ClearMarkFile()函数
\*****************************************************************************/



//定义UNICODE支持
#ifdef UNICODE
	#define __open _wopen
	#define _strlen   wcslen
	#define __rename _wrename
#else
	#define __open _open
	#define _strlen strlen
	#define __rename rename
#endif

#define FILE_PACKET_IDENTIFY	"JZW"


#define FILE_PACKAGE_MAX_LENGTH		(UINT_MAX-10000)

//包文件头1.0
typedef struct _tagPackageHeader_1_0
{
	CHAR	 szPackIdentifier[4];       //包标识 "PAK"
	CHAR	 szPackageName[128];		//包的描述
	DWORD	 dwPackageVersion;			//当前包版本号,当前类支持的版本1.0
	double   dbModifyTime;				
	DWORD	 dwPackageFileVersion;
	DWORD	 dwPackageFilePreVersion;
	DWORD	 dwDelNumberInBytes;///		删除的文件字节数
	BYTE	 dwPackageFileType;			///文件包
	BYTE	 dwSec;						///是否加密
	BYTE	 dwFileState;				///文件状态
	BYTE	 byUnk1;
	DWORD	 dwUnk1;
	BYTE	 secPassword[16][8];		///加密密码
	BYTE	 secPassword2[16][8];		///加密密码
	

	_tagPackageHeader_1_0()
	{
		ZeroMemory(szPackageName,sizeof(szPackageName));
		dwSec = 1;
	}	

	void InitPassword();

}PACKAGE_HEADER_1_0,*PPACKAGE_HEADER_1_0;


//包文件头1.1
typedef struct _tagPackageHeader_1_1 : public _tagPackageHeader_1_0
{
	
	_tagPackageHeader_1_1() : _tagPackageHeader_1_0()
	{
		dbModifyTime = 0;
	}

}PACKAGE_HEADER_1_1,*PPACKAGE_HEADER_1_1;

typedef struct _tagPackageHeader_2_0 : public _tagPackageHeader_1_1
{
	union
	{
		DWORD		dwReserved[128];	///总长度为128*4;
		struct
		{
			DWORD		dwFileInfoOffset;	///
			DWORD	    dwUnk1;
			DWORD		dwFileInfoLength;
			USHORT		dwHeaderLength;	///
			BYTE		btInfoSecIndex;	
		};
	
	};

	_tagPackageHeader_2_0() : _tagPackageHeader_1_1()
	{		
		memset( dwReserved, 0, sizeof(dwReserved) );
	}
} PACKAGE_HEADER_1_2, *PPACKAGE_HEADER_1_2;

//文件头
typedef struct _tagPackageFileHeader
{
	BYTE	  byModify;                 //文件编辑标志，0正常 1新增 2 删除 3标记删除（但数据存在)
	DWORD	 dwUnk1;
	DWORD	 dwUnk2;
	DWORD	 dwUnk3;
	DWORD	 dwUnk4;
	DWORD	 dwUnk5;
	CHAR      szFileName[MAX_PATH];		//文件名
	UINT	  uFileLength;				//文件大小
	unsigned char	  szBufferID[16];			///文件信息
	BOOL      bCompress;				//1 压缩数据 0未压缩
	BYTE	  bSecIndex;
	BYTE	  bSecLen;
	UINT      uAppendInfoLength;	    //文件附加信息长度， <=0 没有附加信息
	DWORD	 dwUnk6;

	_tagPackageFileHeader()
	{
		byModify = 0;
		uFileLength = 0;
		bCompress = FALSE;
		uAppendInfoLength = 0;
		memset( szBufferID, 0, sizeof(szBufferID) );
	}
}PACKAGE_FILE_HEADER;

//文件索引项
typedef struct _tagPackageIndex
{
	DWORD	 dwUnk1;
	DWORD	 dwUnk2;
	DWORD	 dwUnk3;
	DWORD	 dwUnk4;
	CHAR      szFileName[MAX_PATH];		//文件名
	ULONG     lFilePosInPackage;        //文件在包中的位置
	DWORD	 dwUnk5;
	BYTE      byModifyFlag;				//0正常 1新增 2 删除 3标记删除（但数据存在)
	BYTE	  byFileNameLen;
	
	_tagPackageIndex()
	{
		ZeroMemory(szFileName,sizeof(szFileName));
		lFilePosInPackage = 0;
		byModifyFlag = 0;
		byFileNameLen = 0;
	}
}PACKAGE_INDEX,*PPACKAGE_INDEX;

//包中文件信息
typedef struct _tagPackageFileInfo
{
	CHAR     szFileName[MAX_PATH];		//文件名
	unsigned char	  szBufferID[16];
	UINT	  uFileLength;				//文件大小
	BOOL      bCompress;				//1 压缩数据 0未压缩
	BYTE	  bSecIndex;				///实际压缩密码位于bSecIndex－64；小于64未加密
	UINT      uAppendInfoLength;		//文件附加信息长度，0没有附加信息
}PACKAGE_FILE_INFO;


#define GL_FILE_OPEN_READ			0        //打开文件，文件不存在返回错
#define GL_FILE_OPEN_WRITE			1        //打开文件，文件不存在则创建，创建失败返回FALSE

#define GL_FILE_STATE_NORMAL		0	    //正常
#define GL_FILE_STATE_MARK_DEL		3	    //标记删除

#define GL_MOTHED_NONE				0		//文件存在返回错误
#define GL_MOTHED_MARK_DEL			3        //文件存在标记删除,加入当前文件

#define PACKAGE_VERSION_1_0			MAKEWORD(1,0)
#define PACKAGE_VERSION_1_2			MAKEWORD(1,2)

//#define PACKAGE_VERSOIN				PACKAGE_VERSION_1_0
//#define PACKAGE_HEADER				PACKAGE_HEADER_1_0
//#define PPACKAGE_HEADER				PACKAGE_HEADER*
#define SAVE_FLAG_POSITION_1_0		sizeof(PACKAGE_HEADER_1_0)

//#define SAVE_FLAG_POSITION			SAVE_FLAG_POSITION_1_0
#define FILE_START_POSITION_1_0		SAVE_FLAG_POSITION_1_0 + sizeof(BYTE)

//#define FILE_START_POSITION			FILE_START_POSITION_1_0

#define PACKAGE_VERSOIN				PACKAGE_VERSION_1_2
#define PACKAGE_HEADER				PACKAGE_HEADER_1_2
#define PPACKAGE_HEADER				PACKAGE_HEADER*
#define SAVE_FLAG_POSITION_1_2		sizeof(PACKAGE_HEADER_1_2)

#define SAVE_FLAG_POSITION			SAVE_FLAG_POSITION_1_2
#define FILE_START_POSITION_1_2		SAVE_FLAG_POSITION_1_2 + sizeof(BYTE)

#define FILE_START_POSITION			FILE_START_POSITION_1_2


typedef std::map<std::string,PPACKAGE_INDEX>	FileIndexList;
typedef FileIndexList::const_iterator	ConFileIndex;

//压缩数据
//
//参数：
//	pDst				压缩后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳压缩的数据,最坏情况下MAXDestLen=ulSrcLen*0.01+12字节
//	pSrc				待压缩数据地址
//	lenSrc				待压缩数据长度
//	nCompressMethod		压缩算法，-1默认压缩级别，0不压缩，1较快速度压缩，2最快压缩方式，9最高压缩率
//
//	返回：
//	0					失败
//	其它				压缩后数据长度
typedef int (*gpCompressFunc)(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc,int nCompressMethod);

//解压数据
//
//参数：
//	pDst				解压后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳解压的数据
//	pSrc				待解压数据地址
//	lenSrc				待解压数据长度
//
//	返回：
//	0					失败
//	其它				解压后数据长度
typedef int (*gpUncompressFunc)(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc);

#include <oleauto.h>

class CPackageFile
{
public:
	CPackageFile(void);
	~CPackageFile(void);

	//-----------------------------------------------------------------------------------
	// 功  能：打开包文件
	// 参数：
	//		lpszFileName      文件名
	//		dwOpenMode        打开文件方式 GL_FILE_OPEN_WRITE GL_FILE_OPEN_READ
	//	返回值：
	//		成功回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	Open(LPCTSTR lpszFileName, DWORD dwOpenMode = GL_FILE_OPEN_WRITE);

	

	//-----------------------------------------------------------------------------------
	// 功  能：确定文件是否打开
	// 参数：
	//		
	//	返回值：
	//		打开TRUE，否则返回FALSE
	BOOL	IsOpen();

	//-----------------------------------------------------------------------------------
	// 功  能：返回包中文件个数
	// 参数：
	//
	//	返回值：
	//		失败返回-1， 成功返回文件个数
	INT 	GetFileCount();


	//-----------------------------------------------------------------------------------
	// 功  能：向包增加一个文件
	// 参数：
	//		lpszFileName	  文件名
	//		lpszPathName      文件路径
	//		uMothedForExist   文件存在时的操作文件，GL_MOTHED_NONE,GL_MOTHED_MARK_DEL,
	//		lpszAppendInfo    文件附加信息文件
	//		bCompress	      文件是否压缩
	//	返回值：
	//		成功返回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	AddFile(LPCTSTR lpszFileName, LPCTSTR lpszPathName, UINT uMothedForExist = GL_MOTHED_NONE, BOOL bSec=FALSE, LPCTSTR lpszAppendInfoFile = NULL, BOOL bCompress = FALSE);

	//-----------------------------------------------------------------------------------
	// 功  能：向包增加一个文件
	// 参数：
	//		lpszFileName		文件名，不包含路径
	//		pbyFileData			文件内容
	//		uFileDataLen		文件长度
	//		uMothedForExist     文件存在执行的操作，GL_MOTHED_NONE,GL_MOTHED_MARK_DEL
	//		pbyAppendInfoData	文件附加信息，没有附加信息应为NULL
	//		uAppendInfoDataLen	文件附加信息长度
	//		bCompress`			是否进行压缩
	//	返回值：
	//		成功返回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	AddFile(LPCTSTR lpszFileName, BYTE *pbyFileData, UINT uFileDataLen, UINT uMothedForExist = GL_MOTHED_NONE, BYTE *pbyAppendInfoData = NULL, BOOL bSec=FALSE, UINT uAppendInfoDataLen = 0, BOOL bCompress = FALSE);

	//-----------------------------------------------------------------------------------
	// 功  能：从包中删除一个文件
	// 参数：
	//		lpszFileName		文件名，不包含路径
	//		nDelMothed			删除方式 GL_MOTHED_MARK_DEL
	//	返回值：
	//		删除成功返回TRUE，文件不存在或删除失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	DelFile(LPCTSTR lpszFileName, INT nDelMothed = GL_MOTHED_MARK_DEL);

	BOOL	DelAllFile();

	//-----------------------------------------------------------------------------------
	// 功  能：删除包中标记删除的文件，由DelFile RepalceFile 指定 GL_MOTHED_MARK_DEL 产生
	// 参数：
	//		
	//	返回值：
	//		
	//-----------------------------------------------------------------------------------
	void	ClearMarkFile();
	void	RepareContent();

	//-----------------------------------------------------------------------------------
	// 功  能：从包中读一个文件到缓冲区
	// 参数：
	//		lpszFileName		文件名，不包含路径
	//		pbyFileData[out]	函数内部分配，返回文件内存
	//		puFileLen[out]		返回文件长度
	//		pbyAppendInfoData	函数内部分配,返回文件附加信息，如果NULL则不读取文件附回信息
	//		puAppendInfoDataLen	返回文件附加信息长度，如果为NULL则不读取文件附回信息
	//	返回值：
	//		成功返回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	GetFile(LPCTSTR lpszFileName, BYTE** pbyFileData, UINT* puFileLen, BYTE** pbyAppendInfoData = NULL, UINT* puAppendInfoDataLen = NULL);


	VOID	WritePackageFileHeader( const PACKAGE_FILE_HEADER &ph );
	BOOL	ReadPackageFileHeader( PACKAGE_FILE_HEADER &ph,BOOL bEncrypt=TRUE ) const;
	//-----------------------------------------------------------------------------------
	// 功  能：设置包的描述字符串
	// 参数：
	//		lpszTitle			包说明
	//	返回值：
	//		
	//-----------------------------------------------------------------------------------
	void	SetPackageTitle(LPCTSTR lpszTitle);

	//-----------------------------------------------------------------------------------
	// 功  能：返回包的描述字符串
	// 参数：
	//		
	//	返回值：
	//		包说明
	//-----------------------------------------------------------------------------------
	LPCTSTR GetPackageTitle();

	void	SetPackageFileVersion( INT nVersion );
	INT		GetPackageFileVersion();

	void	SetPackageFilePreVersion(INT nVersion );
	INT		GetPackageFilePreVersion();

	void	SetPackageType( BYTE nType );
	BYTE	GetPackageType();

	VOID	EnableSecret( BOOL b);
	BOOL	IsEnableSecret() const	{return m_PackageHeader.dwSec!=0;}

	//VOID	SetCompressLevel( INT nLevel )	{ m_PackageHeader.

	const PACKAGE_HEADER	&GetPackageHeader()	{return m_PackageHeader;}

	void	SavePackageHeaderToDisk();
	//-----------------------------------------------------------------------------------
	// 功  能：保存内存到磁盘
	// 参数：
	//		lpszFileName		文件名
	//	返回值：
	//		成功返回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	Save(LPCTSTR lpszFileName);

	///BUG 修复以前的一些bug
	BOOL	SaveUseIndex(LPCTSTR lpszFileName);


	//-----------------------------------------------------------------------------------
	// 功  能：判断文件在包中是否存在
	// 参数：
	//		lpszFileName		文件名，不包含路径
	//	返回值：
	//		文件存在返回TRUE，否则返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL    FileExist(LPCTSTR lpszFileName);
	BOOL	IsSameFile( const PACKAGE_FILE_INFO &fInfo );

	//-----------------------------------------------------------------------------------
	// 功  能：并闭文件，释放内存
	// 参数：
	//		
	//	返回值：
	//		
	//-----------------------------------------------------------------------------------
	void    Close();

	//-----------------------------------------------------------------------------------
	// 功  能：返回包文件起始位置，用于GetNextFile
	// 参数：
	//		
	//	返回值：
	//		如果不存在文件，返回NULL
	//-----------------------------------------------------------------------------------
	//POSITION GetFirstFilePosition();
	ConFileIndex GetFirstFilePosition();
	ConFileIndex GetFileIndex( LPCTSTR pFilename );

	BOOL IsEndFilePosition(ConFileIndex cit);

	BOOL GetFileInfo(ConFileIndex cit, PACKAGE_FILE_INFO &fInfo);
	//-----------------------------------------------------------------------------------
	// 功  能：读取下一个位置文件
	// 参数：
	//		pos			文件位置 ,用GetFirstFile GetNextFile 返回
	//	返回值：
	//		文件内容，些指针要由用户释放
	//-----------------------------------------------------------------------------------
	//PACKAGE_FILE_INFO* GetNextFile(POSITION& pos);

	//-----------------------------------------------------------------------------------
	//  功  能：检测当前版本是不是类支持的最新版本
	//	返回值：
	//		不是最新版返回TRUE，否则返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL   IsNeedUpdate();
	//-----------------------------------------------------------------------------------
	//  功  能：如果当前版本不是类支持的最新版本则升级文件
	//	返回值：
	//		不用升级或升级成功返回TRUE，否则返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL    UpdatePackageFile();

	BOOL	SavePacketV12IndexInfo(INT nReInitIndexLie = -1);

protected:
	
	double GetSystemTime()
	{
		SYSTEMTIME systm;
		double     variant;
		SystemTimeToVariantTime(&systm, &variant);
		return variant;
	}


	//-----------------------------------------------------------------------------------
	// 打开包文件
	// 参数：
	//		hFile			  已经打开的文件句柄
	//	返回值：
	//		成功回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL    Open(INT   hFile);
	//-----------------------------------------------------------------------------------
	// 从指定的位置读取指定长度的数据
	// 参数：
	//		hFile			已经打开的文件句柄
	//		uRead			要读取的字节数，UINT_MAX 从文件开始处读取整个文件内容
	//		lReadPosition	读取的开始位置，UINT_MAX 从当前位置开始读
	//	返回值：
	//		文件内容，些指针要由用户释放，如果读取失败返回NULL
	//-----------------------------------------------------------------------------------
	BYTE*	Read(INT hFile, UINT uRead = UINT_MAX, UINT lReadPosition = UINT_MAX) const;

	INT		ReadTo(INT hFile, BYTE *pBufferTo, UINT nBufferLength, UINT uRead = UINT_MAX, UINT lReadPosition = UINT_MAX );


	//-----------------------------------------------------------------------------------
	// 在指定的位置写入指定长度的数据
	// 参数：
	//		hFile			已经打开的文件句柄
	//		pBuf			要写入的数据
	//		uBufLen			数据长度
	//		lWritePosition	写入位置，UINT_MAX在当前位置写入
	//	返回值：
	//		成功返回TRUE，失败返回FALSE
	//-----------------------------------------------------------------------------------
	BOOL	Write(INT hFile, const void* pBuf, UINT uBufLen, UINT lWritePosition = UINT_MAX);

	//-----------------------------------------------------------------------------------
	// 复制内存对像
	// 参数：
	//		pBuf			待复制的数据
	//		uBufLen			数据长度
	//	返回值：
	//		成功返回数据的复本，失败返回NULL
	//-----------------------------------------------------------------------------------
	inline PVOID CopyData(PVOID pBuf, UINT uBufLen);

	//-----------------------------------------------------------------------------------
	// 写入开始更新标记
	// 参数：
	//
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	inline void	BeginSave(INT hFile = -1);


	//-----------------------------------------------------------------------------------
	// 写入完成更新标记
	// 参数：
	//
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	inline void	EndSave(INT hFile = -1);


	//-----------------------------------------------------------------------------------
	// 保存包头信息到磁盘
	// 参数：
	//
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	BOOL	SavePackageHeader(INT hFile = -1);

	//-----------------------------------------------------------------------------------
	// 从包内容创建文件索引表
	// 参数：
	//
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	BOOL    CreateFileIndex();


	BOOL	LoadPackageV12FileIndex();


	//-----------------------------------------------------------------------------------
	// 从索引文件读入文件索引表
	// 参数：
	//
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	//BOOL	LoadFileIndex(INT hFile = -1);

	//-----------------------------------------------------------------------------------
	// 更新文件索引表
	// 参数：
	//		pIndex 标识要更新的文件，如果为NULL 则更新整个文件索引表
	//	返回值：
	//		成功返回TRUE 失败返回FALSE
	//-----------------------------------------------------------------------------------
	//BOOL	SaveFileIndex(INT hFile = -1, const PACKAGE_INDEX* pIndex = NULL);

	//-----------------------------------------------------------------------------------
	// 获取文件的索引文件名
	// 参数：
	//		lpszFileName`	文件名
	//		lpIndexFile		out 索引文件名
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	//void    GetIndexFileName(const char* lpszFileName, char* lpIndexFile);

	//-----------------------------------------------------------------------------------
	// 获取文件的索引文件名
	// 参数：
	//		lpszFileName`	文件名
	//		lpBackupFile	out 索引文件名
	//	返回值：
	//
	//-----------------------------------------------------------------------------------
	void    GetBackupFileName(const char* lpszFileName, char* lpBackupFile);

	void	SetCompressFunc( gpCompressFunc pCompress, gpUncompressFunc pUnCompress )
	{
		m_pFuncCompress = pCompress;
		m_pFuncUnCompress = pUnCompress;
	}
public:
	const FileIndexList& GetFileIndexList()
	{
		return m_PackFileIndex;
	}

protected:

	//文件保存状态，文件打开时从文件读入，0 正常 ,1 正在写入 ，如果是1则文件没有完成存盘操作，要重建文件索引表
	BYTE           m_bySaveFlags;				

	//当前文件句柄
	INT            m_hFile;


	//索引文件句柄
	//INT			   m_hIndexFile;
	
	//当前打开的文件
	TCHAR          m_szPathName[MAX_PATH];

	//包头信息
	PACKAGE_HEADER m_PackageHeader;

	//包中文件列表
	FileIndexList		m_PackFileIndex;
	//CPtrList			m_PackFileIndex;

	//是否是以只读方式打开文件
	BOOL		   m_bReadOpen;


	gpCompressFunc		m_pFuncCompress;
	gpUncompressFunc	m_pFuncUnCompress;

};
