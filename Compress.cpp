#include "StdAfx.h"
#include "Compress.h"

///#include "zlib_lib/zlib.h"

typedef int (*PCOMPRESS)(char *pDst,UINT lenDst,const char *pSrc,UINT lenSrc,int nCompressMethod);
typedef int (*PUNCOMPRESS)(char *pDst,UINT lenDst,const char *pSrc,UINT lenSrc);

static HINSTANCE s_hZip=NULL;
static CRITICAL_SECTION s_csZip;
static BOOL bLoadFail=FALSE;

static PCOMPRESS s_pCompress=NULL;
static PUNCOMPRESS s_pUncompress=NULL;

//装入压缩和解压包GLZip.dll
static BOOL LoadZip()
{/*
	if (s_hZip!=NULL) return TRUE;
	if (bLoadFail) return FALSE;

	static BOOL bFirstIn=TRUE;
	if (bFirstIn)
	{
		InitializeCriticalSection(&s_csZip);
		bFirstIn=FALSE;
	}

	EnterCriticalSection(&s_csZip);
	if (s_hZip!=NULL)
	{
		//万一出现两个线程都想来load时，后面的那个自动返回成功
		LeaveCriticalSection(&s_csZip);
		return TRUE;
	}
	char	szTmp[MAX_PATH];
	//if (gl_szGameSharePath[0])
	//{
	//	wsprintf(szTmp,"%s\\GLCompress.dll",gl_szGameSharePath);
	//}
	//else
	{
		wsprintf(szTmp,"yfzc.dll");
	}
	s_hZip=LoadLibrary(szTmp);
	if (s_hZip==NULL)
	{
		bLoadFail=TRUE;
		LeaveCriticalSection(&s_csZip);
		return FALSE;
	}
	//查出压缩和解压的函数地址
	s_pCompress=(PCOMPRESS)GetProcAddress(s_hZip,"GL_ZIPCompress");
	s_pUncompress=(PUNCOMPRESS)GetProcAddress(s_hZip,"GL_ZIPUncompress");
	if (s_pCompress==NULL || s_pUncompress==NULL)
	{
		bLoadFail=TRUE;
		FreeLibrary(s_hZip);
		s_hZip=NULL;
		LeaveCriticalSection(&s_csZip);
		return FALSE;
	}
	LeaveCriticalSection(&s_csZip);*/
	return TRUE;
}

//压缩数据
//
//参数：
//	pDst				压缩后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳压缩的数据,最坏情况下MAXDestLen=ulSrcLen*0.01+12字节
//	pSrc				待压缩数据地址
//	lenSrc				待压缩数据长度
//	nCompressMethod		压缩算法，-1默认压缩级别，0不压缩，1较快速度压缩，2最快压缩方式，9最高压缩率
//
//	返回：
//	0					成功
//	其它				失败、、压缩后数据长度
//int gpCompress(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc,int nCompressMethod)
//{
//	if (!LoadZip()) return 0;

	//HZIP zipHandle;
	//int n = ZipCreateBuffer( &zipHandle, pSrc, lenSrc, NULL );
	//if( n!=0 )
	//	return;
	//n = ZipGetMemory( zipHandle, &pDest, &lenDst, NULL );
	//ZipClose( zipHandle )

		///ZLIb Version
	//uLongf len = lenDst;
	//int n = compress2((Bytef *)pDst,&len,(const Bytef *)pSrc,lenSrc,nCompressMethod);
	//lenDst = len;
	//return n;
//}

//解压数据
//
//参数：
//	pDst				解压后数据存放缓冲区
//	lenDst				pDst的长度，必须足够大能容纳解压的数据
//	pSrc				待解压数据地址
//	lenSrc				待解压数据长度
//
//	返回：
//	0					失败
//	其它				解压后数据长度
//int gpUncompress(char *pDst,UINT &lenDst,const char *pSrc,UINT lenSrc)
//{
//	if (!LoadZip()) return 0;
	//HUNZIP zipHandle;
	//int n = UnzipOpenBuffer( &zipHandle, pSrc, lenSrc, NULL );
	//if( n != ZR_OK )
	//	return n;
	//UnzipItemToBuffer( );
	//ZipClose( zipHandle );

	///ZLIb Version
	//uLongf len = lenDst;
	//int n = uncompress((Bytef *)pDst,&len,(const Bytef *)pSrc,lenSrc);
	//lenDst = len;
	//return n;
//}

//压缩数据
//
//调用：
//	pBuf			需要压缩的数据地址
//	lenBuf			调用时是待压缩数据的长度，若压缩成功返回时是压缩后的数据长度
//	nZipMethod		压缩方法
//	cnTopReserve	在压缩后的数据缓冲区前部保留多少个字节的空间，留作其它用途
//
//返回：
//	若不压缩或者申请内存失败或压缩失败，返回NULL，其它表示压缩后的缓冲区地址，调用者需要释放这块缓冲区
void *gpZipBuf(gpCompressFunc pFunc, void *pBuf,int &lenBuf,int nZipMethod,int cnTopReserve)
{
	//数据不存在或者数据过小，不用压缩
	if (pBuf==NULL || lenBuf<=100 || pFunc==NULL) return NULL;

	//不需要压缩
	if (nZipMethod==GL_ZIPMETHOD_NONE) return NULL;

	//压缩后数据的格式：
	//dwOldLen		原数据长度
	//压缩数据

	//为了确保压缩成功执行，缓冲区大小必须比未压缩数据略大些，由GLZipLib决定
	UINT lenDst=lenBuf+lenBuf/100+12;
	char *pTmp=(char *)malloc(cnTopReserve+sizeof(DWORD)+lenDst);
	if (pTmp==NULL)
	{
		//内存不足
		return NULL;
	}

	//调用压缩函数
	int nRet=(*pFunc)(pTmp+cnTopReserve+sizeof(DWORD),lenDst,(char *)pBuf,lenBuf,nZipMethod);
		//gpCompress(pTmp+cnTopReserve+sizeof(DWORD),lenDst,(char *)pBuf,lenBuf,nZipMethod);
	if (nRet!=0)
	{
		//压缩失败
		free(pTmp);
		return NULL;
	}
	//原数据长度
	*(DWORD *)(pTmp+cnTopReserve)=lenBuf;
	//压缩后的数据长度
	lenBuf=lenDst+sizeof(DWORD);
	return pTmp;
}

//解压缩数据
//
//调用：
//	pBuf		压缩数据地址，必须是gpZipBuf函数生成的压缩数据
//	lenBuf		调用时是压缩数据的长度，若解压成功返回解压后的数据长度
//
//返回：
//	当解压缩成功时返回解压数据的地址，否则返回NULL，解压后的数据需要调用者释放
void *gpUnzipBuf(gpUncompressFunc pFunc,void *pBuf,int &lenBuf)
{
	if( pFunc == NULL )
		return NULL;
	//原始数据大小
	int len=*(DWORD *)pBuf;

	char *pTmp=(char *)malloc(len);
	if (pTmp==NULL)
	{
		//内存不足
		return NULL;
	}

	UINT nUnLen = len;
	//调用解压函数
	int nRet=(*pFunc)(pTmp,nUnLen,((char *)pBuf)+sizeof(DWORD),lenBuf-sizeof(DWORD));
		//gpUncompress(pTmp,nUnLen,((char *)pBuf)+sizeof(DWORD),lenBuf-sizeof(DWORD));
	if (nRet!=0)
	{
		//压缩失败
		free(pTmp);
		return NULL;
	}
	if (nUnLen!=len)
	{
		//解压后的数据竟然与原来压缩时说明的长度不一致一定是有问题的
	}
	//还原后的数据长度
	lenBuf=nRet;
	return pTmp;
}
