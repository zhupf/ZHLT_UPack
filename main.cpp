#include "stdafx.h"
#include "PackageFile.h"
#include "RWFile.h"
#include "FileUtils.h"

#include <io.h>
#include <direct.h>
#include <dbghelp.h>
#include <shlwapi.h>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
using namespace FileUtils;

#pragma comment(lib, "dbghelp")
#pragma comment(lib, "shlwapi")
int packdir(string& path)
{
	if (path[path.length()-1] == '\\')
	{
		path.resize(path.length() - 1);
	}
	string packfileName = path + ".jzw";
	CPackageFile kPackageFile;
	if (!kPackageFile.Open(packfileName.c_str(), GL_FILE_OPEN_WRITE))
	{
		printf("packdir cannot create .jzw!");
		return -1;
	}
	vector<file_t> files;
	FileUtils::SearchFiles(path.c_str(), files);
	string kBuff;
	for (size_t i = 0; i < files.size(); i++)
	{
		file_t& file = files[i];
		if (!file.isDir)
		{
			if (RWFile::LoadFile(file.fullpath, kBuff))
			{
				kPackageFile.AddFile(file.filename, kBuff.c_str(), kBuff.size(), NULL, 0, TRUE);
			}
		}
	}
	kPackageFile.Close();
	printf("packdir OK!!!");
	return 0;
}

int depack(string& path)
{
	if (path.length() < 5)
	{
		printf("depack test.jzw");
		return -1;
	}
	if (path.substr(path.length() - 4, 4) != ".jzw")
	{
		printf("depack test.jzw");
		return -1;
	}
	CPackageFile kPackageFile;
	if (!kPackageFile.Open(path.c_str(), GL_FILE_OPEN_READ))
	{
		printf("depack cannot open .jzw!");
		return -1;
	}
	string dir = path;
	dir.resize(dir.length()-4);
	dir += "\\";
	::MakeSureDirectoryPathExists(dir.c_str());
	const FileIndexList& kIndexList = kPackageFile.GetFileIndexList();
	ConFileIndex itLie = kIndexList.begin();
	ConFileIndex itEnd = kIndexList.end();
	for( ;itLie!=itEnd; ++itLie )
	{
		BYTE* byFileData = NULL;
		UINT uFileLen = 0;
		if (kPackageFile.GetFile(itLie->second->szFileName, &byFileData, &uFileLen))
		{
			RWFile::SaveFile(dir + itLie->second->szFileName, byFileData, uFileLen);
			free(byFileData);
		}
	}
	printf("depack OK!!!");
	return 0;
}
int main(int argc, char **argv)
{
	printf("uPackage.exe v1.0 by zhupf QQ:335264417\n");
	printf("uPackage.exe for �л�����!!!\n");
	if (argc == 2)
	{
		string path = argv[1];
		if (!PathFileExists(path.c_str()))
		{
			printf("file not found!!!");
			return -1;
		}
		if (PathIsDirectory(path.c_str()))
		{
			return packdir(path);
		}
		else
		{
			return depack(path);
		}
	}
	else
	{
		printf("uPackage.exe dir or jzw!!!\n");
	}
	::system("pause");
	return 0;
}